const path = require('path');
const {
    VueLoaderPlugin
} = require('vue-loader');

module.exports = {
    entry: './src/index.js',
    module: {
        rules: [{
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    "plugins": [
                        "syntax-dynamic-import",
                    ]
                }
            }, {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'] //  <=  Order is very important
            }, {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
};