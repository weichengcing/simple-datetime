import date from "./datetime.vue";
Vue.component(
    "date",
    Vue.extend({
        mixins: [date],
        props: {
            format: "YYYY-MM-DD"
        }
    })
);